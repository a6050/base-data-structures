package arrays;

import com.google.common.base.Stopwatch;
import org.junit.Assert;
import org.junit.Test;

public class VectorArrayTest {

    @Test
    public void testAdd() {
        int[] sizes = {1000, 10000, 100000, 1000000, 10000000, 100000000};

        for (int size : sizes) {
            VectorArray<String> array = new VectorArray<>();
            Stopwatch stopwatch = Stopwatch.createStarted();
            for (int i = 0; i < size; i++) {
                array.add("a");
            }
            stopwatch.stop();
            System.out.printf("size: %d, time: %s", size, stopwatch);
            System.out.println();
        }
    }

    @Test
    public void testInsert() {
        VectorArray<String> array = new VectorArray<>();
        for (int i = 0; i < 10; i++) {
            array.add("a" + i);
        }

        array.insert("a55", 5);
        Assert.assertEquals("a55", array.get(5));

        array.insert("a00", 0);
        Assert.assertEquals("a00", array.get(0));

        array.insert("a100", array.count());
        Assert.assertEquals("a100", array.get(array.count() - 1));

        array = new VectorArray<>();
        for (int i = 0; i < 9; i++) {
            array.add("a" + i);
        }

        array.insert("a55", 5);
        Assert.assertEquals("a55", array.get(5));

        array.insert("a00", 0);
        Assert.assertEquals("a00", array.get(0));

        array.insert("a100", array.count());
        Assert.assertEquals("a100", array.get(array.count() - 1));
    }

    @Test
    public void testRemove() {
        VectorArray<String> array = new VectorArray<>();
        for (int i = 0; i < 11; i++) {
            array.add("a" + i);
        }

        array.remove(5);
        Assert.assertEquals("a6", array.get(5));

        array.remove(0);
        Assert.assertEquals("a1", array.get(0));

        array.remove(array.count() - 1);
        Assert.assertEquals("a9", array.get(array.count() - 1));
    }

    @Test
    public void testRemoveLast() {
        VectorArray<String> array = new VectorArray<>();
        for (int i = 0; i < 11; i++) {
            array.add("a" + i);
        }

        array.removeLast();
        Assert.assertEquals("a9", array.get(array.count() - 1));

        array.removeLast();
        Assert.assertEquals("a8", array.get(array.count() - 1));

        array.removeLast();
        Assert.assertEquals("a7", array.get(array.count() - 1));
    }
}