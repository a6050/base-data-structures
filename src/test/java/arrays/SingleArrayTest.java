package arrays;

import com.google.common.base.Stopwatch;
import org.junit.Assert;
import org.junit.Test;

public class SingleArrayTest {

    @Test
    public void testAdd() {
        int[] sizes = {1000, 10000, 100000, 1000000, 10000000, 100000000};

        for (int size : sizes) {
            SingleArray<String> singleArray = new SingleArray<>();
            Stopwatch stopwatch = Stopwatch.createStarted();
            for (int i = 0; i < size; i++) {
                singleArray.add("a");
            }
            stopwatch.stop();
            System.out.printf("size: %d, time: %s", size, stopwatch);
            System.out.println();
        }
    }

    @Test
    public void testGet() {
        SingleArray<String> singleArray = new SingleArray<>();
        for (int i = 0; i < 10; i++) {
            singleArray.add("a" + i);
        }

        String value = singleArray.get(5);
        Assert.assertEquals("a5", value);
    }

    @Test
    public void testInsert() {
        SingleArray<String> singleArray = new SingleArray<>();
        for (int i = 0; i < 10; i++) {
            singleArray.add("a" + i);
        }

        singleArray.insert("a55", 5);
        Assert.assertEquals("a55", singleArray.get(5));

        singleArray.insert("a00", 0);
        Assert.assertEquals("a00", singleArray.get(0));

        singleArray.insert("a100", singleArray.count());
        Assert.assertEquals("a100", singleArray.get(singleArray.count() - 1));
    }

    @Test
    public void testRemove() {
        SingleArray<String> singleArray = new SingleArray<>();
        for (int i = 0; i < 10; i++) {
            singleArray.add("a" + i);
        }

        singleArray.remove(5);
        Assert.assertEquals("a6", singleArray.get(5));

        singleArray.remove(0);
        Assert.assertEquals("a1", singleArray.get(0));

        singleArray.remove(singleArray.count() - 1);
        Assert.assertEquals("a8", singleArray.get(singleArray.count() - 1));
    }
}