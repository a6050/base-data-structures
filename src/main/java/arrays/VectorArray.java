package arrays;

public class VectorArray<T> implements Array<T> {

    private T[] array;
    private int size;
    private final int vector;

    public VectorArray() {
        this(10);
    }

    public VectorArray(int vector) {
        this.array = (T[]) new Object[0];
        this.size = 0;
        this.vector = vector;
    }

    @Override
    public int count() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public void add(T item) {
        if (count() == array.length) {
            resize();
        }
        array[size] = item;
        size++;
    }

    @Override
    public void insert(T item, int index) {
        T[] newArray = (T[]) new Object[array.length + (count() == array.length ? vector : 0)];
        if (!isEmpty()) {
            System.arraycopy(array, 0, newArray, 0, index);
            System.arraycopy(array, index, newArray, index + 1, size - index);
        }
        newArray[index] = item;
        size++;
        array = newArray;
    }

    @Override
    public void remove(int index) {
        T[] newArray = (T[]) new Object[array.length - (size == array.length - vector + 1 ? vector : 0)];
        if (!isEmpty()) {
            System.arraycopy(array, 0, newArray, 0, index);
            if (index != size - 1) {
                System.arraycopy(array, index + 1, newArray, index, size - index - 1);
            }
        }
        size--;
        array = newArray;
    }

    @Override
    public void removeLast() {
        if (size == array.length - vector + 1) {
            T[] newArray = (T[]) new Object[array.length - vector];
            if (!isEmpty()) {
                System.arraycopy(array, 0, newArray, 0, size - 1);
            }
            array = newArray;
        } else {
            array[size - 1] = null;
        }
        size--;
    }

    @Override
    public T get(int index) {
        return array[index];
    }

    private void resize() {
        T[] newArray = (T[]) new Object[count() + vector];
        if (!isEmpty()) {
            System.arraycopy(array, 0, newArray, 0, count());
        }
        array = newArray;
    }
}
