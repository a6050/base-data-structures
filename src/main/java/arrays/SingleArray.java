package arrays;

public class SingleArray<T> implements Array<T> {

    private T[] array;

    public SingleArray() {
        array = (T[]) new Object[0];
    }

    @Override
    public int count() {
        return array.length;
    }

    @Override
    public boolean isEmpty() {
        return array.length == 0;
    }

    @Override
    public void add(T item) {
        resize(count() + 1);
        array[array.length - 1] = item;
    }

    @Override
    public void insert(T item, int index) {
        T[] newArray = (T[]) new Object[count() + 1];
        newArray[index] = item;
        System.arraycopy(array, 0, newArray, 0, index);
        System.arraycopy(array, index, newArray, index + 1, array.length - index);
        array = newArray;
    }

    @Override
    public void remove(int index) {
        T[] newArray = (T[]) new Object[count() - 1];
        System.arraycopy(array, 0, newArray, 0, index);
        System.arraycopy(array, index + 1, newArray, index, newArray.length - index);
        array = newArray;
    }

    @Override
    public void removeLast() {
        resize(count() - 1);
    }

    @Override
    public T get(int index) {
        return array[index];
    }

    private void resize(int newSize) {
        T[] newArray = (T[]) new Object[newSize];
        System.arraycopy(array, 0, newArray, 0, Math.min(array.length, newArray.length));
        array = newArray;
    }
}
