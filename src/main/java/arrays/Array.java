package arrays;

public interface Array<T> {

    int count();

    boolean isEmpty();

    /**
     * добавить элемент в конец массива
     *
     * @param item
     */
    void add(T item);

    void insert(T item, int index);

    void remove(int index);

    /**
     * удалить элемент в конце массива
     */
    void removeLast();

    /**
     * вернуть элемент по указанному индексу
     *
     * @param index индекс массива
     * @return
     */
    T get(int index);
}
